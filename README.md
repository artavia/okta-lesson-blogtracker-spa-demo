# Okta Lesson Blogtracker Spa Demo

## Description
This is a re&#45;hash of a write&#45;up found at the okta blog by Braden Kelley.

### Processing and/or completion date(s)
  - July 21, 2020 to July 29, 2020
    - This is embarassing. It was as done as it was going to be on Sunday, July 26, 2020. However, since I attempted to cure the development defects that are described below, the clock did not officially stop until today, Wednesday, July 29, 2020.

## Attribution(s)
The original blog article called [Build a Basic CRUD App with Node and React](https://developer.okta.com/blog/2018/07/10/build-a-basic-crud-app-with-node-and-react "link to okta blog") can be found at the okta blog. 

## What is different?
The arrangement of the stack is different. I keep the client and server separate from each other. My point is to build on previous precedent that I have found comfortable to work with. With that said, there should have been far less comments and personal notes because I still am treading over the same old beaten path and have already left myself breadcrumbs on a previous occasion. The truth, however, is that **I am always learning**, thus, for the moment the notes and comments will stay in.

## A small observation
Material-UI is a memory hog and there is no way to fix this while in development. When the project on the client side, however, has been compiled the application is, on the other hand, **as quick as mercury in a frying pan**.

## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the 
[ApolloGraphQL fullstack tutorial](https://www.apollographql.com/docs/tutorial/introduction/ "link to tutorial") (or, **AFT** for short) on two different occasions. I am building on previous precedent in order to successfully finish that original hands&#45;on exercise found in apollographql introduction tutorial.

I thank you for your interest.

## What&rsquo;s next?
I will see the following subjects in the next few days (and likely weeks):
  - GraphQL (there are two additional exercises that I have located which will help me warm up for the ApolloGraphQL fullstack tutorial);
  - Testing Behavior Driven Development (I will be re&#45;visiting the subjects of **Jest, Mocha, Chai and Enzyme**);
  - MomentJS (this is low&#45;hanging fruit but a necessary stop I should make);

## Caveat Emptor
Use at your own risk. While I took the path less traveled and decided to not &quot;future proof&quot; the exercise with locked in package versions (opting for the &quot;latest&quot; versions instead), I did cure a lot of bugs in the process of doing so. However, there are three React Dev warnings of which I am aware at the moment that I may or may not address at this time. My theory is that the **recompose** package glosses over too much of the finer work contained within.  

I am running out of time to be frank but these are the issues:
  - During login I generate the following **warning** (it seems to be an issue with **material-ui** regardless of whether the **@okta/okta-signin-widget** is utilized);
    - &quot;Unhandled Rejection (AuthSdkError): The app should not attempt to call authorize API on callback. Authorize flow is already in process. Use parseFromUrl() to receive tokens.&quot;
  - During logout I generate the following **warning**: 
    - &quot;Warning: findDOMNode is deprecated in StrictMode. findDOMNode was passed an instance of Transition which is inside StrictMode.&quot;
  - During the moment I decide to edit a record I generate the following **warning**: 
    - &quot;Warning: Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in the componentWillUnmount method.&quot;

If, indeed, I decide to pick one bug and fix it it will likely be the sign&#45;in bug regarding the **&quot;Unhandled Rejection&quot;**. I just have to change the composition of the sign&#45;in function and it should go away altogether.

**Update:** I ended up chasing my tail until I finally became thoroughly sick of trying to figure out some of the bugs. 

## I&rsquo;m Audi 5000! 
Have fun with it because I am passing the baton on to you! 

I ran the production version in the node server upon compiling the build version of the React project on the client side. And, if I didn't know better **only one warning** remained. 

### God bless