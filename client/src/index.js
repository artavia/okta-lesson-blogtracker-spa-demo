import React from 'react';
import ReactDOM from 'react-dom';

import "fontsource-roboto/latin-300.css";
import "fontsource-roboto/latin-400.css";
import "fontsource-roboto/latin-500.css";
import "./styles/roboto.css";

import {App} from './App';

import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


// add this per the instructions -- turn on hot module reloading, which will make it so that changes you make automatically update in the app without needing to refresh the whole page

// if( module.hot ){ module.hot.accept(); }