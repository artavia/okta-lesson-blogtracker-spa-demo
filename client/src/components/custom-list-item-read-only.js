import React from 'react';

import { Typography, ListItem, ListItemText } from "@material-ui/core"; 

import moment from "moment";

const CustomListItemReadOnly = ( { baselabel, post } ) => {

  const { title, body, updatedAt } = post;
  
  if( ( baselabel === "class" ) ){
    
    let elNoEdit = (
      <ListItem>
  
        <ListItemText 
          primary={title} 
          secondary={ 
            <>
              <Typography component="span" variant="body1" style={{ display: "block" }}> { body } </Typography>
              <Typography component="span" variant="body2" style={{ display: "block" }} gutterBottom> { updatedAt && `Updated ${ moment( updatedAt).fromNow() }` } </Typography>
            </>
          } 
        />
  
      </ListItem>
    );
    
    return elNoEdit;
  }  

};

export { CustomListItemReadOnly };