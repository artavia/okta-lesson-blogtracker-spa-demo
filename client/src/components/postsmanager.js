import React from "react";

import { withRouter, Route, Redirect } from "react-router-dom";
import { NavLink } from "react-router-dom";

// import { useHistory } from 'react-router-dom';

import { withOktaAuth } from "@okta/okta-react";

import { withStyles } from "@material-ui/core"; 
// import { makeStyles } from "@material-ui/core/styles"; // this is a HOOK, hence, it cannot be applied here...

import { Paper, List } from "@material-ui/core"; 
import { Typography } from "@material-ui/core"; 

import { Fab } from "@material-ui/core";

import { Add as AddIcon } from "@material-ui/icons";

import { find, orderBy } from "lodash";

// import { compose } from "recompose";

import { PostEditor } from "./../layout/posteditor";
import { ErrorSnackbar } from "./../layout/errorsnackbar";

import { CustomListItem } from "./custom-list-item";

const styles = ( theme ) => ( {
  posts: {
    marginTop: theme.spacing(2)
  }
  , fab: {
    position: "fixed",  // position: "absolute",
    bottom: theme.spacing(3)
    , right: theme.spacing(3)
    , [theme.breakpoints.down('xs')]: {
      bottom: theme.spacing(2)
      , right: theme.spacing(2)
    },
  }

} );

class ClassyPostsManager extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      loading: true
      , posts: []
      , error: null
    };
    this.baseUrl = `/posts`;
    this.postsManagerPageUrl = `/admin`;

    // this.props.history = useHistory();

    this.customfetch = this.customfetch.bind(this);
    this.getPosts = this.getPosts.bind(this);
    this.savePost = this.savePost.bind(this);
    this.deletePost = this.deletePost.bind(this);
    this.renderPostEditor = this.renderPostEditor.bind(this);

    this.paperElement = this.paperElement.bind(this);
    this.noDataDisclosure = this.noDataDisclosure.bind(this);
    this.orderTheResults = this.orderTheResults.bind(this);
    this.dataList = this.dataList.bind(this);
    this.mapData = this.mapData.bind(this);
    
  }

  async componentDidMount(){
    await this.getPosts();
  }

  // PROVISIONALLY COMMENTED
  async componentDidUpdate(){
    await this.getPosts();
  }

  paperElement(){
    let el = (
      <Paper elevation={1} className={ this.props.classes.posts }>
        <List>
          { this.dataList() }
        </List>
      </Paper>
    );
    return el;
  }

  noDataDisclosure(){
    let el = (<Typography variant="subtitle1">No posts to display!</Typography>);
    return el;
  }

  loadingDisclosureElement(){
    let el = (<Typography variant="subtitle1">Your request is pending and loading&hellip;</Typography>);
    return el;
  }
  
  orderTheResults(){
    let shaped = orderBy( this.state.posts, ["updatedAt", "title" ], ["desc", "asc"]  );
    return shaped;
  }
  
  dataList(){
    return this.orderTheResults().map( this.mapData );
  }

  // /POSTS || /ADMIN
  mapData(post){

    return <CustomListItem 
      key={ post.id }
      baselabel={ "class" }
      post={ post }
      postsManagerPageUrl={ this.postsManagerPageUrl }
      deletePost={ this.deletePost }
    />;

  }

  async customfetch( method, endpoint, body ){
    
    try{
      const fetchObject = {
        method: method
        , body: JSON.stringify( body )
        , headers: {
          "Content-type" : "application/json; charset=UTF-8"
          , "Accept" : "application/json"
          , "Authorization" : `Bearer ${ await this.props.authState.accessToken }`
        }
      };

      const response = await fetch( `${endpoint}`, await fetchObject );
      const data = await response.json();
      
      // console.log( "this.customfetch() await data" , await data );

      return await data;
    }
    catch( error ){
      console.error( "error" , error );
      this.setState( { error: error } );
    }

  }

  async getPosts(){

    const allposts = await this.customfetch( "get" , this.baseUrl );
    // console.log( "this.getPosts() await allposts" , await allposts );

    this.setState( {
      loading: await false
      , posts: await allposts
    } );
    
  }

  async savePost( post ){
    
    // console.log( "\n\n this.savePost(post) ~ post \n\n" , post );

    if( post.id ){
      await this.customfetch("put", `${this.baseUrl}/${post.id}` , post );
      // const editedpost = await this.customfetch("put", `${this.baseUrl}/${post.id}` , post );
      // console.log( "\n\n this.savePost(post) PUT METHOD ~ await editedpost \n\n" , await editedpost );
    }
    else {
      await this.customfetch("post", `${this.baseUrl}`, post );
      // const newpost = await this.customfetch("post", `${this.baseUrl}`, post );
      // console.log( "\n\n this.savePost(post) POST METHOD ~ await newpost \n\n" , await newpost );
    }

    // await this.props.history.goBack(); 
    await this.props.history.push(`${this.postsManagerPageUrl}`);
    await this.getPosts();
    
  }

  async deletePost( post ){
    
    await this.customfetch("delete", `${this.baseUrl}/${post.id}` );

    await this.getPosts();

  }

  renderPostEditor( { match: { params: { id } } } ){  
    
    if( this.state.loading ){
      return null;
    } 
    
    const post = find( this.state.posts , { id: id } ); 

    // /POSTS || /ADMIN
    if( !post && id !== "new" ){ 
      return <Redirect to={`${this.postsManagerPageUrl}`} />; 
    }

    return <PostEditor post={ post } onSave={ this.savePost } />;
  }

  render(){  
    
    return (
      <>
        <Typography variant="h4">Posts Manager</Typography>

        { this.state.loading && this.loadingDisclosureElement() }
        
        { !this.state.loading && this.state.posts.length === 0 && this.noDataDisclosure() }
        
        { !this.state.loading && this.state.posts.length > 0 && this.paperElement() }

        {/* /POSTS || /ADMIN */}
        <Fab color="secondary" aria-label="add" className={ this.props.classes.fab } component={NavLink} to={ `${this.postsManagerPageUrl}/new` } >
          <AddIcon />
        </Fab>

        {/* /POSTS || /ADMIN */}
        <Route exact path={`${this.postsManagerPageUrl}/:id`} render={ this.renderPostEditor } />

        { this.state.error && (<ErrorSnackbar onClose={ () => this.setState( { error: null } ) } message={ this.state.error.message } />) }

      </>
    );

  }
  
};

// const PostsManager = compose( withOktaAuth, withRouter, withStyles(styles) )( ClassyPostsManager );

const EenieMeenie = withStyles(styles)(ClassyPostsManager);
const MineyMoe = withRouter( EenieMeenie );
const PostsManager = withOktaAuth( MineyMoe );

export { PostsManager };