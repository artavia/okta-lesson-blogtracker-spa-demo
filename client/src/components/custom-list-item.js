import React from 'react';
import { NavLink } from "react-router-dom";

import { Typography, IconButton, ListItem, ListItemText, ListItemSecondaryAction } from "@material-ui/core"; 
import { Delete as DeleteIcon } from "@material-ui/icons";

import moment from "moment";

const CustomListItem = ( { baselabel, post, postsManagerPageUrl, deletePost } ) => {

  const { id, title, body, updatedAt } = post;
  
  if( ( baselabel === "class" ) ){
    
    let elEdit = (
      <ListItem button component={NavLink} to={`${postsManagerPageUrl}/${id}`}>
  
        <ListItemText 
          primary={title} 
          secondary={ 
            <>
              <Typography component="span" variant="body1" style={{ display: "block" }}> { body } </Typography>
              <Typography component="span" variant="body2" style={{ display: "block" }} gutterBottom> { updatedAt && `Updated ${ moment( updatedAt).fromNow() }` } </Typography>
            </>
          } 
        />
  
        <ListItemSecondaryAction>
          <IconButton onClick={() => deletePost(post) } color="inherit" >
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
  
      </ListItem>
    );
    
    return elEdit;
  }

};

export { CustomListItem };