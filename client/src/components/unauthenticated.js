import React from "react";
import { useState } from "react";
import { useEffect, useCallback } from "react";

import { Typography, Paper, Grid } from "@material-ui/core";

import * as OktaSignIn from "@okta/okta-signin-widget";
import "@okta/okta-signin-widget/dist/css/okta-sign-in.min.css";

import { REACT_APP_OKTA_ORG_URL } from "../custom-okta-security-configuration/development-environment-variables";

import SVENGALLIO from "./../svg/logo.svg";

import { securityConfigurationObject } from "../custom-okta-security-configuration/config";

const { clientId, issuer, redirectUri, scopes, pkce } = securityConfigurationObject.oidcsecurityrouting;

const { display, responsetype } = securityConfigurationObject.otherconfigurationvalues;
// const { baseUrl, authorizeurl, responsemode } = securityConfigurationObject.otherconfigurationvalues;

const Unauthenticated = () => {

  const [ user, setUser ] = useState(false);

  const innerFunction = useCallback( () => {
    const goGetEm = async () => {

      const newSignInConfig = {
        
        baseUrl: REACT_APP_OKTA_ORG_URL  // "your Okta Domain is the only required option to get started"
        , clientId: clientId // required property in general
        , redirectUri: redirectUri // required property to this object 
        , el: "#signIn"
        , logo: `${SVENGALLIO}`
        
        , authParams: {
          pkce: pkce 
          , issuer: issuer
          , display: display 
          , responseType: responsetype

          // , responseMode: "form_post" 
          // , responseMode: responsemode 

          // , responseMode: pkce ? "query" : "fragment"    
          // , responseMode: "query" // pkce === true 
          // , responseMode: "fragment" // pkce === false 

          // , scopes: scopes 
        }

      };

      const signIn = new OktaSignIn( newSignInConfig );
      
      /* const signInConfig = {
        baseUrl: REACT_APP_OKTA_ORG_URL
        , el: "#signIn"
        , logo: `${SVENGALLIO}`
        , authParams: {
          pkce: pkce
          , issuer: issuer
        }
      };
      
      const signIn = new OktaSignIn( signInConfig ); */ 
  
      return await Promise.resolve( signIn );
  
    };

    goGetEm().then( async (signIn) => {

      signIn.remove();

      const getTokensConfig = {
        clientId: clientId // required property in general
        , redirectUri: redirectUri // required property to this object 
        , getAccessToken: true  // Return an access token from the authorization server 
        , getIdToken: true  // Return an ID token from the authorization server
        , scope: "openid profile"
      };

      signIn.showSignInToGetTokens( getTokensConfig );

      return signIn;

      
    } )
    .then( ( signIn ) => {

      const authClient = signIn.authClient;
      const session = authClient.session.get();
      return { authClient, session, signIn };

    } )
    .then( async ({authClient, session, signIn }) => {
      
      if( await session.status !== undefined ){
        console.log( ">>>>> await session.status: \n\n" , await session.status );
      }
  
      // Session exists, show logged in state
      if( await session.status === "ACTIVE" ){
        
        // set username in state
        setUser( await session.login );
        await localStorage.setItem( "isAuthenticated" , "true" );
        
        console.log( ">>>>> await user \n\n" , await user );
  
        // get access and ID tokens
        await authClient.token.getWithoutPrompt( {
          scopes: scopes
        } )
        .then( async ( tokens ) => {
          
          console.log( ">>>>> await tokens \n\n" , await tokens );
          
          await tokens.forEach( async (token) => {
            
            if( await token.idToken ){
              await authClient.tokenManager.add( "idToken", token);
            }
            if( await token.accessToken ){
              await authClient.tokenManager.add( "accessToken", token);
            }
  
          } );
  
          await authClient.tokenManager.get("idToken").then( ( idToken ) => {
            
            console.log( `>>>>> Hello, \n\n ${idToken.claims.name} (${idToken.claims.email})` );
  
            // window.location.reload(); // THIS IS A HACK... REDO 
          } );
  
        } )
        .catch( ( error ) => {
          console.error( "thennable catch statement error" , error );
        } );
  
        return;
      }
      
    } );

  } , [ user ] ); // Update if user changes
  
  useEffect( () => { 
    
    innerFunction();
    return () => {};

  } , [ innerFunction ] ); // Update if innerFunction changes
  
  const el = (
    <Grid container spacing={0} direction="column" alignItems="center" justify="center" style={{ minHeight: "100vh" }}>

      <Grid item>
        <Typography variant="h6" gutterBottom style={{ textAlign: "center" }}>Please login, babe!</Typography>
        <Typography variant="subtitle1" gutterBottom style={{ textAlign: "center" }} >You need to sign in to continue. After you <strong>login, then, you can visit</strong> the restricted areas.</Typography>
      </Grid>

      <Grid item>
        <Paper id="signIn" elevation={2}></Paper>
      </Grid>

    </Grid>
  );

  return el;
};

export { Unauthenticated };