// CLASS BASED COMPONENT

import React from "react";

import { Typography, Paper, Grid, List } from "@material-ui/core";

import { orderBy } from "lodash";

import { withOktaAuth } from "@okta/okta-react";

import { CustomListItemReadOnly } from "./custom-list-item-read-only";

class ClassyHome extends React.Component {

  constructor(props){
    
    super(props);

    this.baseUrl = `/blogpreview`;

    this.state = { posts: [] }; 

    this.getPosts = this.getPosts.bind( this );    
    this.paperElement = this.paperElement.bind(this);
    this.noDataDisclosure = this.noDataDisclosure.bind(this);

    this.loadingDisclosureElement = this.loadingDisclosureElement.bind(this);
    this.elPending = this.elPending.bind(this);
    this.orderTheResults = this.orderTheResults.bind(this);
    this.dataList = this.dataList.bind(this);
    this.mapData = this.mapData.bind(this);
    this.elNotAuthenticated = this.elNotAuthenticated.bind( this );
    this.elAuthenticated = this.elAuthenticated.bind(this);
  }

  async componentDidMount(){ 
    await this.getPosts();    
  }

  getPosts(){
    // console.log("getPosts()");

    let getRequest = new Request( `${this.baseUrl}` );

    let getHeaders = new Headers();
    getHeaders.append( "Content-type" , "application/json; charset=UTF-8" );
    getHeaders.append( "Accept" , "application/json" );

    const getoptionsobject = {
      method: "GET"
      , headers: getHeaders
    };
    
    return fetch( getRequest , getoptionsobject )
    .then( (response) => {
      return response.json();
    } )
    .then( ( data ) => {
      
      // console.log( "\n\n data \n\n" , data );

      const { posts } = data;
      this.setState( { posts: posts } );

    } );

  }

  paperElement(){
    let el = (
      <Paper elevation={2}>
        <List>
          { this.dataList() }
        </List>
      </Paper>
    );
    return el;
  }

  noDataDisclosure(){
    let el = (<Typography variant="subtitle1" style={{ textAlign: "center" }}>No posts to display!</Typography>);
    return el;
  }

  loadingDisclosureElement(){
    let el = (<Typography variant="subtitle1" style={{ textAlign: "center" }}>Your request is pending and loading&hellip;</Typography>);
    return el;
  }

  elPending(){
    let el = (<Typography variant="subtitle1" style={{ textAlign: "center" }}>Your request is pending and loading&hellip;</Typography>);
    return el;
  }
  
  orderTheResults(){
    let shaped = orderBy( this.state.posts, ["updatedAt", "title" ], ["desc", "asc"]  );
    return shaped;
  }
  
  dataList(){
    return this.orderTheResults().map( this.mapData );
  }

  mapData( post ){
    
    return <CustomListItemReadOnly 
      key={ post.id }
      baselabel={ "class" }
      post={ post }
    />;

  }

  elNotAuthenticated(){
    return (
      <>
        <Typography variant="h3" gutterBottom style={{ textAlign: "center" }}>You are not signed in yet.</Typography>
        <Typography variant="subtitle1" gutterBottom style={{ textAlign: "center" }}>By all means please login!</Typography>
      </>
    );
  }

  elAuthenticated(){
    return (
      <>
        <Typography variant="h3" gutterBottom style={{ textAlign: "center" }}>You are authenticated, baby!!!</Typography>
      </>
    );
  }

  render(){ 

    const el = (
      <Grid container spacing={0} direction="column" alignItems="center" justify="center" style={{ minHeight: "100vh" }}>
  
        <Grid item>
          
          <Typography variant="h2" gutterBottom style={{ textAlign: "center" }}>Welcome home!</Typography>

          { !this.props.authState.isAuthenticated && this.elNotAuthenticated() }
          { this.props.authState.isAuthenticated && this.elAuthenticated() }
          { this.props.authState.isPending && this.elPending() }

        </Grid>
  
        <Grid item>
          
          <Typography variant="h4" gutterBottom style={{ textAlign: "center", textTransform: "uppercase" }}>Your blog posts!</Typography>
          { this.state.loading && this.loadingDisclosureElement() }
          { !this.state.loading && this.state.posts.length === 0 && this.noDataDisclosure() }
          { !this.state.loading && this.state.posts.length > 0 && this.paperElement() }

        </Grid>
  
      </Grid>
    );
  
    return el;

  }
  
};

const Home = withOktaAuth( ClassyHome );
export { Home };