import React from "react";

import { Snackbar, SnackbarContent, IconButton } from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";

import { Error as ErrorIcon, Close as CloseIcon } from "@material-ui/icons";

const useStyles = makeStyles( theme => ( {

  snackbarContent: {
    backgroundColor: theme.palette.error.dark
  }
  , message: {
    display: "flex"
    , alignItems: "center"
  }
  , icon: {
    fontSize: 20
  }
  , iconVariant: {
    opacity: 0.9
    , marginRight: theme.spacing(1)
  }
} ) );

const ErrorSnackbar = (props) => {
  
  const { id, message, onClose } = props;
  
  const deezClasses = useStyles();

  let el = (
    <Snackbar open autoHideDuration={5625} onClose={ onClose }>
      <SnackbarContent 
        className={`${deezClasses.margin} ${deezClasses.snackbarContent}`} 
        aria-describedby={id} 
        message={ <span id={id} className={deezClasses.message} ><ErrorIcon className={`${deezClasses.icon} ${deezClasses.iconVariant}`} />&nbsp;{message}</span>} 
        action={ [ <IconButton key="close" aria-label="Close" color="inherit" onClick={ onClose }><CloseIcon className={deezClasses.icon} /></IconButton> ] } 
      />
    </Snackbar>
  );

  return el;
};

export { ErrorSnackbar };