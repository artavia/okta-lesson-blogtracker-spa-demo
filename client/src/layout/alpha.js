import React from 'react';

import { BrowserRouter, Route, Switch /*, Redirect*/ } from "react-router-dom";
import { useHistory } from 'react-router-dom';

import { Security , LoginCallback , SecureRoute } from "@okta/okta-react";

import { securityConfigurationObject } from "./../custom-okta-security-configuration/config";

import { REACT_APP_REDIRECT_PATH } from "./../custom-okta-security-configuration/development-environment-variables";

import { Home } from "./../components/home";

import { PostsManager } from "./../components/postsmanager";

import { Unauthenticated } from "./../components/unauthenticated";
import { DNEPage } from "./../DNE/DNEPage";

import { Main } from "./main";

const HasAccessToBrowserRouter = () => {
  
  const history = useHistory();
  const customizedSignInWidget = () => {
    history.push("/unauthenticated");
  };

  const element = (
    <Security { ...securityConfigurationObject.oidcsecurityrouting } onAuthRequired={ customizedSignInWidget } >
      <Main>
        <Switch>
          
          {/* FROM THE TOP DOWN THE ORDER MUST STAY THE SAME... */}

          <Route exact={ true } path={ REACT_APP_REDIRECT_PATH } component={ LoginCallback } />
          <Route exact={ true } path="/" component={ Home } />
          
          {/* /POSTS || /ADMIN */}
          <SecureRoute path="/admin" component={ PostsManager } />
          {/* <SecureRoute exact={ true } path="/admin" component={ PostsManager } /> */}

          <Route exact={true} path="/unauthenticated" component={ Unauthenticated }/>

          {/* Handling 404s ( either/or ) - change the url */}            
          {/* <Route path="/404" component={ DNEPage } /> */}
          {/* <Redirect from="*" to="/404" /> */}
          
          {/* Handling 404s ( either/or ) - do not change the url */}
          <Route component={ DNEPage } />

        </Switch>
      </Main>
    </Security>
  );

  return element;
};

const Alpha = () => { 
  let el = (
    <BrowserRouter>
      <HasAccessToBrowserRouter />
    </BrowserRouter>
  );
  return el;
};

export { Alpha };