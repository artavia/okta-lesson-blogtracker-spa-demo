import React from "react";

import { Button, IconButton, Menu, MenuItem, ListItemText } from "@material-ui/core";
import { AccountCircle } from "@material-ui/icons";
import { withOktaAuth } from "@okta/okta-react";

class ClassyLoginButton extends React.Component {

  constructor(props){
    super( props );
    this.state = {
      authenticated: null
      , user: null
      , menuAnchorEl: null
    };

    this.checkAuthentication = this.checkAuthentication.bind(this);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleMenuOpen = this.handleMenuOpen.bind(this);
    this.handleMenuClose = this.handleMenuClose.bind(this);
  }

  componentDidUpdate(){
    this.checkAuthentication();
  }

  componentDidMount(){
    this.checkAuthentication();
  }

  async checkAuthentication(){
    const authenticated = await this.props.authState.isAuthenticated;
    if( await authenticated !== this.state.authenticated ){
      const user = await this.props.authService.getUser();
      this.setState( {
        authenticated: await authenticated
        , user: await user
      } );
    }
  }
  
  async login( event ){ 
    event.preventDefault();
    // console.log( "this.login" );

    // /POSTS || /ADMIN
    // await this.props.authService.login(); 
    await this.props.authService.login("/admin"); 
  }
  
  async logout( event ){ 
    event.preventDefault();
    // console.log( "this.logout" );
    await this.handleMenuClose();

    // uri parameter is optional
    await this.props.authService.logout("/"); 

    // CULL THE COOKIES care of... https://stackoverflow.com/questions/179355/clearing-all-cookies-with-javascript
    
    document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });

    // CULL ANY LOCALSTORAGE
    
    window.localStorage.clear();

  }

  handleMenuOpen(event){
    this.setState( {
      menuAnchorEl: event.currentTarget
    } );
  }
  
  handleMenuClose(){
    this.setState( {
      menuAnchorEl: null
    } );
  }
  
  render(){
    
    if( this.state.authenticated === null ){
      return null;
    }

    if( !this.state.authenticated ){
      return <Button color="inherit" onClick={ this.login } >Login</Button>;
    }

    const menuPosition = {
      vertical: "top"
      , horizontal: "right"
    };

    let el = (
      <div>

        <IconButton onClick={ this.handleMenuOpen } color="inherit">
          <AccountCircle />
        </IconButton>

        <Menu anchorEl={ this.state.menuAnchorEl } anchorOrigin={ menuPosition } transformOrigin={ menuPosition } open={ !!this.state.menuAnchorEl } onClose={ this.handleMenuClose } >

          <MenuItem onClick={ this.logout }>
            <ListItemText primary="Logout" secondary={ this.state.user && this.state.user.name } />
          </MenuItem>

        </Menu>

      </div>
    );

    return el;
  }
  
};

const LoginButton = withOktaAuth( ClassyLoginButton );
export { LoginButton };