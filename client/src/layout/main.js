import React from 'react';

import { CssBaseline } from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";

import { AppHeader } from "./appheader";

const useStyles = makeStyles( theme => ( {
  main: {
    padding: theme.spacing(3)
    , [ theme.breakpoints.down('xs') ]: {
      padding: theme.spacing(2)
    }
  }
} ) );

const Main = ( props ) => { 
  
  const { children } = props;
  
  const deezClasses = useStyles();

  let el = (
    <>
      <CssBaseline />
      <AppHeader />
      <main className={ deezClasses.main }>
        { children }
      </main>
    </>
  );
  return el;
};

export { Main };