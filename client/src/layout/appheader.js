import React from "react";
import { NavLink } from "react-router-dom";

import { AppBar, Toolbar, Typography, Button } from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";

import { LoginButton } from "./loginbutton";

import { useOktaAuth } from "@okta/okta-react";

const useStyles = makeStyles( {
  flex: {
    flex: 1
  }
} );

const AppHeader = () => {

  const deezClasses = useStyles();

  const { authState } = useOktaAuth();

  let el = (
    <AppBar position="static">
      <Toolbar>
        
        <Typography variant="h6" color="inherit">
          My React App
        </Typography>
        
        <Button color="inherit" component={NavLink} to={"/"} >Home</Button>
        
        {/* /POSTS || /ADMIN */}
        { authState.isAuthenticated && <> 
          <Button color="inherit" component={NavLink} to={"/admin"} >Posts Manager</Button>
        </> }

        {/* <Button color="inherit" component={NavLink} to={"/moarrrrtest"} >MOARRR!</Button> */}

        {/* <div className={ classes.flex }></div> */}
        <div className={ deezClasses.flex }></div>

        <LoginButton />

      </Toolbar>
    </AppBar>
  );

  return el;
};

export { AppHeader };
