import React from "react";

import { useHistory } from 'react-router-dom';

import { Card, CardContent, CardActions, Modal, Button, TextField } from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";

import { Form, Field } from "react-final-form";

const useStyles = makeStyles( theme => ( {
  modal: {
    display: "flex"
    , alignItems: "center"
    , justifyContent: "center"
  }
  , modalCard: {
    width: "90%"
    , maxWidth: 500
  }
  , modalCardContent: {
    display: "flex"
    , flexDirection: "column"
  }
  , marginTop: {
    marginTop: theme.spacing(2)
  }
} ) );

const PostEditor = ( props ) => {
  
  const { post, onSave } = props;

  const history = useHistory();

  const deezClasses = useStyles();
  
  let el = ( 
    <Form initialValues={post} onSubmit={ onSave } >
      { ( { handleSubmit } ) => (
        <Modal className={ deezClasses.modal } onClose={ () => history.goBack() } open >
          
          <Card className={ deezClasses.modalCard }>
            <form onSubmit={handleSubmit}>
              
              <CardContent className={deezClasses.modalCardContent}>
                <Field id="title" name="title">
                  { ({ input }) => <TextField label="Title" autoFocus {...input} /> }
                </Field>
                <Field id="body" name="body">
                  { ({ input }) => <TextField className={ deezClasses.marginTop } label="Body" multiline rows={4} {...input} /> }
                </Field>
              </CardContent>

              <CardActions>
                <Button size="small" color="primary" type="submit">Save</Button>
                <Button size="small" onClick={ () => history.goBack() } >Cancel</Button>
              </CardActions>

            </form>
          </Card>

        </Modal>
      ) }
    </Form>
  );
  return el;
};

export { PostEditor };