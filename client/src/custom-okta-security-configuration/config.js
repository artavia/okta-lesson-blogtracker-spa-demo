import { 
  REACT_APP_OKTA_ORG_URL
  , REACT_APP_OKTA_CLIENT_ID
  , REACT_APP_REDIRECT_URI 
  , REACT_APP_POSTLOGOUT_REDIRECT_URI 

  // , REACT_APP_OKTA_TESTING_DISABLEHTTPSCHECK

} from "./development-environment-variables";

const securityConfigurationObject = {
  
  oidcsecurityrouting: {
    
    // required property in signIn.showSignInToGetTokens config object
    clientId: REACT_APP_OKTA_CLIENT_ID 

    //  Defaults to the baseUrl plus "/oauth2/default"
    , issuer: `${REACT_APP_OKTA_ORG_URL}/oauth2/default`

    // required property signIn config object
    , redirectUri: REACT_APP_REDIRECT_URI 

    // Valid OIDC scopes: openid, email, profile, address, phone 
    // , scopes: [ "openid", "email" ]  // DEFAULT value 
    , scopes: [ "openid", "profile", "email" ]  // OVERRIDE value
    
    // "The PKCE OAuth flow will be used by default." ~ see https://github.com/okta/okta-auth-js#pkce-oauth-20-flow

    // "Implicit flow can be enabled by setting the pkce option to false" ~ see https://github.com/okta/okta-auth-js#implicit-oauth-20-flow
    
    , pkce: true  // DEFAULT VALUE
    // , pkce: false    // TESTING PURPOSES 

    , postLogoutRedirectUri: REACT_APP_POSTLOGOUT_REDIRECT_URI

    // , disableHttpsCheck: REACT_APP_OKTA_TESTING_DISABLEHTTPSCHECK, // do not utilize this!!!
  }

  , otherconfigurationvalues: {
    
    // "your Okta Domain is the only required option to get started"
    // baseUrl: `${REACT_APP_OKTA_ORG_URL}`, 

    // authorizeUrl Defaults to the issuer plus "/v1/authorize"
    // , authorizeurl: `${REACT_APP_OKTA_ORG_URL}/oauth2/default/v1/authorize`,
    
    // OVERRIDE default and Redirect to the authorization server as indicated by redirectUri
    display: "page"

    
    // Valid response types are id_token, access_token, and code
    , responsetype: [ "id_token", "token" ] // Default value 
    // , responsetype: "token" // OVERRIDE the default value
    // , responsetype: "code" // OVERRIDE the default value


    // SAVAGE COMMENT concerning authParams.responseMode
    // , responseMode: pkce ? "query" : "fragment"

    // search "authParams.display = " at https://github.com/okta/okta-signin-widget#openid-connect
        
    // on authParams.responseMode: 
    // "query" is the default value for standard web applications where authParams.responseType = 'code'. 
    // For SPA applications, the default will be query if using PKCE, or fragment for implicit OIDC flow.


    // , responsemode: "query"


    // "fragment" is the default for Single-page applications using the implicit OIDC flow and for standard web applications where responseType != 'code'. 
    // SPA Applications using PKCE flow can set responseMode = 'fragment' to receive the authorization code in the hash fragment instead of the query.


    // , responsemode: "fragment"

    // Use form_post instead of query in the Authorization Code flow
    // authParams.responseMode = "form_post" - Returns the authorization response as a form POST after the authorization redirect. 
    // Use this when authParams.display = page and you do not want the response returned in the URL. 

    // , responsemode: "form_post" 

  }
  
};

export { securityConfigurationObject };