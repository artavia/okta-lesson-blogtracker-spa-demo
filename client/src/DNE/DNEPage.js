import React from "react";

const DNEPage = ( {location} ) => {
  
  const el = (
    <>
      <div>
        <h1 className="display-3">404, babe!</h1>
        <h2>No match found for <code>{location.pathname}</code></h2>
      </div>
      <div>
        <p>Page Not Found.</p>
      </div>
    </>
  );

  return el;
};

export { DNEPage };
