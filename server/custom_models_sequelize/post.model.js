const sequelize = require("./../custom_db/database-obj")

const { DataTypes } = require("sequelize");
// const { Sequelize } = require("sequelize");

const { v4: uuidv4 } = require("uuid");

console.log( "Let's get this show on the road!!!\n\n" );

// =============================================
// MODEL CREATION PHASE FOR MAPPING
// https://sequelize.org/master/manual/model-basics.html#model-definition
// =============================================

const Post = sequelize.define( "posts" , { 
  id: {
    type: DataTypes.STRING
    , allowNull: false  
    , primaryKey: true
    , defaultValue: uuidv4()
  } , 
  title: {
    type: DataTypes.STRING
    , allowNull: false
  }
  , body: {
    type: DataTypes.TEXT
    , allowNull: false
  }
} );

module.exports = Post;